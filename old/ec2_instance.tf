resource "aws_instance" "inst1" {
    ami = "AMI_ID"
    instace_type = "t2.medium"
    subnet_id = "subnedID"
    associate_public_ip_address = true
    key_name = "keyName" 
    tags = {
        Name = "server1"
        Owner = "server"
    }
}


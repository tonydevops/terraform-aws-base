provider "aws" {
    region = var.region
}

resource "aws_instance" "inst1" {
    ami = var.amiID
    instance_type = var.type
    # subnet_id = "subnet-62606418"
    associate_public_ip_address = true
    key_name = var.key 
    user_data = file("userdata.sh")
    root_block_device {
        volume_size = "40"
        volume_type = "standard"
  }

    tags = {
        Name = "server1"
        Owner = "server"
    }

    provisioner "remote-exec" {
    inline = [
      "echo 'certificate details for key yada yada yda'  > /etc/pki/out-trust/anchors/next.pem",
      "aws s3  s3://files/dist/jenkins.war /root --sse aws:kms",
      "cd /root/dist; unzip /opt/gradle  gradle-2.2-bin.zip  ; unzip /opt/gradle  gradle-2.9-bin.zip  ; ls -lR /opt/gradle > /root/gradle.log",
      "mkdir /var/lib/jenkins ;\
      cd /root/ && tar xvf jenkins-save.tar > /root/jenkins-save-restore.log ; \
      nohup java -jar /home/.../jar/server-process-0.35.jar prod >> /var/../server-process-prod.log 2>&1 ",
      ]
  }

}
